package com.example.pc.doverweight.Interfaces;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.pc.doverweight.R;
import com.example.pc.doverweight.SqliteHelper.ConexionSQliteHelperAl;
import com.example.pc.doverweight.entidades.Almuerzos;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class c_Almuerzos extends AppCompatActivity {
    ArrayList<String> listaCalorias= new ArrayList<>();
    ArrayList<String> result= new ArrayList<>();
    ArrayList<Almuerzos> listaDesayunos;
    ListView lv1;
    ConexionSQliteHelperAl conn;
    ArrayAdapter <String> adapter;
    private ListView listView;
    private ArrayAdapter<String> listAdapter;
    List<String> x = new ArrayList();
    List<String> y = new ArrayList();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c__almuerzos);
        lv1 = (ListView) findViewById(R.id.r_Almuerzo);
        lv1.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Se busca la referencia del TextView en la vista.
                TextView textView = (TextView) view.findViewById(R.id.txtNombreRaw);
                TextView textViewcal = (TextView) view.findViewById(R.id.txtCaloriasRaw);
                //Obtiene el texto dentro del TextView.
                String textItemList  = textView.getText().toString();
                String textItemListcal  = textViewcal.getText().toString();


                x.add(String.valueOf(textItemList));
                y.add(textItemListcal);
            }
        });

        conn = new ConexionSQliteHelperAl(getApplicationContext(), "almuerzos", null, 1);
        datosInicio();
    }

    public void datosInicio(){
        lv1 = (ListView) findViewById(R.id.r_Almuerzo);
        ArrayList<Product> products = new ArrayList<>();
        SQLiteDatabase db=conn.getReadableDatabase();
        Almuerzos almuerzos=null;
        listaDesayunos=new ArrayList<Almuerzos>();
        Cursor cursor=db.rawQuery("SELECT * FROM "+Almuerzos.NOMBRE_TABLA_ALMUERZOS,null);
        while (cursor.moveToNext()){
            Object name;
            almuerzos=new Almuerzos();
            String nombre = almuerzos.setNombre(cursor.getString(0));
            int calori = almuerzos.setCalorias(Integer.parseInt(cursor.getString(1)));
            listaDesayunos.add(almuerzos);
            products.add(new Product(nombre, calori));
        }

        lv1.setAdapter(new ListProductAdapter(this, products));

    }

    static class ListProductAdapter extends BaseAdapter {
        private final Context context;
        private final ArrayList<c_Almuerzos.Product> products;

        public ListProductAdapter(Context context, ArrayList<c_Almuerzos.Product> products) {
            this.context = context;
            this.products = products;
        }

        @Override
        public int getCount() {
            return products.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

            c_Almuerzos.Product product = products.get(position);
            c_Almuerzos.ListProductAdapter.ListViewHolder holder;
            if (view == null) {
                view = inflater.inflate(R.layout.list_almuerzo_row, viewGroup, false);
                holder = new ListProductAdapter.ListViewHolder();
                holder.txtName = (TextView) view.findViewById(R.id.txtNombreRaw);
                holder.txtDescripcion = (TextView) view.findViewById(R.id.txtCaloriasRaw);

                view.setTag(holder);
            } else {
                Log.d("ListView", "RECYCLED");
                holder = (c_Almuerzos.ListProductAdapter.ListViewHolder) view.getTag();
            }

            holder.txtName.setText(product.name);
            holder.txtDescripcion.setText(Integer.toString(product.datos));
            return view;
        }

        static class ListViewHolder {
            TextView txtName;
            TextView txtDescripcion;
        }
    }
    public class Product {
        String name;
        int datos;

        public Product(String name, int datos) {
            this.name = name;
            this.datos = datos;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, NuevoAlimento.class);
            startActivity(intent);
        }
        if (id == R.id.action_sig) {
            Intent intent = new Intent(this, Filtrar.class);
            intent.putExtra("miLista", (Serializable) x);
            intent.putExtra("miLista2", (Serializable) y);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

}