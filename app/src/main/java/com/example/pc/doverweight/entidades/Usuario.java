package com.example.pc.doverweight.entidades;

public class Usuario {

    private String name, apellido, edad, email, estatura, peso;

    public String getEstatura() {
        return estatura;
    }

    public String setEstatura(String estatura) {
        this.estatura = estatura;
        return estatura;
    }

    public String getPeso() {
        return peso;
    }

    public String setPeso(String peso) {
        this.peso = peso;
        return peso;
    }

    public Usuario(String name, String apellido, String edad, String email, String estatura, String peso ) {
        this.name = name;
        this.apellido = apellido;
        this.edad = edad;
        this.email = email;
        this.estatura = estatura;
        this.peso = peso;
    }

    public Usuario() {
        this.name = name;
        this.apellido = apellido;
        this.edad = edad;
        this.email = email;
        this.estatura = estatura;
        this.peso = peso;
    }

    public String getName() {
        return name;
    }

    public String setName(String name) {
        this.name = name;
        return name;
    }

    public String getApellido() {
        return apellido;
    }

    public String setApellido(String apellido) {
        this.apellido = apellido;
        return apellido;
    }

    public String getEdad() {
        return edad;
    }

    public String setEdad(String edad) {
        this.edad = edad;
        return edad;
    }

    public String getEmail() {
        return email;
    }

    public String setEmail(String email) {
        this.email = email;
        return email;
    }
}


