package com.example.pc.doverweight.Interfaces;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.pc.doverweight.R;
import com.example.pc.doverweight.SqliteHelper.ConexionSQliteHelperCaloriasT;
import com.example.pc.doverweight.entidades.CaloriasTotales;

import java.util.ArrayList;
import java.util.List;

public class EstadoDieta extends AppCompatActivity {
    Cursor cur;
    TextView ultIngreso,estado,calOptim;
    ListView listCalIngre;
    ArrayList<CaloriasTotales> listaDesayunos;
    ConexionSQliteHelperCaloriasT conn;
    List<String> y = new ArrayList();
    int sum;
    String Usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estado_dieta);
        conn = new ConexionSQliteHelperCaloriasT(getApplicationContext(), "caloriast", null, 1);
        datosInicio();

        ultIngreso = (TextView)findViewById(R.id.calUltiIngre);
        listCalIngre = (ListView) findViewById(R.id.listiCalEstado);
        estado = (TextView)findViewById(R.id.showEstado);
        calOptim = findViewById(R.id.calOptimas);

        listCalIngre.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textViewcal = (TextView) view.findViewById(R.id.txtCaloriasRaw);
                String textItemListcal  = textViewcal.getText().toString();
                y.add(textItemListcal);
            }
        });
        SharedPreferences sharedPreferences =
                getSharedPreferences("calMomento", Context.MODE_PRIVATE);
        Usuario = sharedPreferences.getString("CaloriasMomento","");
        ultIngreso.setText(Usuario+" Kcal");
        MensajeDieta();

    }

    public void datosInicio(){
        listCalIngre = (ListView) findViewById(R.id.listiCalEstado);
        ArrayList<EstadoDieta.Product> products = new ArrayList<>();
        SQLiteDatabase db=conn.getReadableDatabase();
        CaloriasTotales caloriasTotales=null;
        listaDesayunos=new ArrayList<CaloriasTotales>();
        Cursor cursor=db.rawQuery("SELECT * FROM caloriast",null);
        // order by id  desc
        while (cursor.moveToNext()){
            Object name;
            caloriasTotales=new CaloriasTotales();
            int calori = caloriasTotales.setCalorias(Integer.parseInt(cursor.getString(0)));
            listaDesayunos.add(caloriasTotales);
            products.add(new Product(calori));
        }

        listCalIngre.setAdapter(new ListProductAdapter(this, products));

    }
    @SuppressLint("ResourceAsColor")
    public void MensajeDieta(){
        SharedPreferences sharedPreferences =
                getSharedPreferences("datosperfil", Context.MODE_PRIVATE);
        String edadS = sharedPreferences.getString("edadP","");
        String pesoS = sharedPreferences.getString("pesoP","");
        String estaturaS = sharedPreferences.getString("estaturaP","");

        double suma = (double) ((10 * Double.parseDouble(pesoS))+(6.25 * Double.parseDouble(estaturaS))-(5 * Double.parseDouble(edadS)));
        calOptim.setText(String.valueOf(suma)+" Kcal");


        double sum1 = suma+300;
        double sum2 = suma-200;
        if (Double.parseDouble(Usuario) <= (sum1) && Double.parseDouble(Usuario) >= (sum2)){
            estado.setText("DIETA EQUILIBRADA");
            estado.setTextColor(Color.parseColor("#4CAF50"));
        }
        if (Double.parseDouble(Usuario) >= (sum1)){
            estado.setText("DIETA EXEDIDA");
            estado.setTextColor(Color.parseColor("#B71C1C"));
        }if (Double.parseDouble(Usuario) <= (sum2)) {
            estado.setText("DIETA DEFICIENTE");
            estado.setTextColor(Color.parseColor("#B71C1C"));
        }
    }
    static class ListProductAdapter extends BaseAdapter {
        private final Context context;
        private final ArrayList<EstadoDieta.Product> products;

        public ListProductAdapter(Context context, ArrayList<EstadoDieta.Product> products) {
            this.context = context;
            this.products = products;
        }

        @Override
        public int getCount() {
            return products.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

            EstadoDieta.Product product = products.get(position);
            EstadoDieta.ListProductAdapter.ListViewHolder holder;
            if (view == null) {
                view = inflater.inflate(R.layout.list_calorias_row, viewGroup, false);
                holder = new ListProductAdapter.ListViewHolder();
                holder.txtDescripcion = (TextView) view.findViewById(R.id.txtCaloriasRaw);

                view.setTag(holder);
            } else {
                Log.d("ListView", "RECYCLED");
                holder = (EstadoDieta.ListProductAdapter.ListViewHolder) view.getTag();
            }

            holder.txtDescripcion.setText(Integer.toString(product.datos));
            return view;
        }

        static class ListViewHolder {
            TextView txtDescripcion;
        }
    }
    public class Product {
        int datos;

        public Product( int datos) {
            this.datos = datos;
        }
    }

}
