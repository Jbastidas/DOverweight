package com.example.pc.doverweight.Interfaces;

import android.app.ProgressDialog;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.pc.doverweight.R;
import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;

import java.math.BigInteger;
import java.security.SecureRandom;

public class CrearCuenta extends AppCompatActivity implements View.OnClickListener {
    Button btnRegistrar;
    ImageView imageViewFoto;
    Uri imageUri;
    private static final int PICK_IMAGE = 100;
    EditText TextEmail, TextPassword, Textnombre, textApellido, textEdad,textPeso,textEstatura;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    Firebase firebase;
    FirebaseAuth.AuthStateListener listener;
    private StorageReference mStorage;
    private CheckBox opcionMostrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);
        btnRegistrar = (Button) findViewById(R.id.ButtonGuardarCuenta);
        TextEmail = (EditText) findViewById(R.id.txtCorreoC);
        TextPassword = (EditText) findViewById(R.id.txtContraC);
        Textnombre= findViewById(R.id.txtNombre);
        textApellido= findViewById(R.id.txtApellido);
        textEdad= findViewById(R.id.txtEdad);
        textPeso= findViewById(R.id.txtPeso);
        textEstatura= findViewById(R.id.txtEstatura);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        btnRegistrar = (Button) findViewById(R.id.ButtonGuardarCuenta);
        opcionMostrar = (CheckBox)findViewById(R.id.opcion_mostrar);


        progressDialog = new ProgressDialog(this);
        btnRegistrar.setOnClickListener(this);
        firebase.setAndroidContext(this);
        firebase = new Firebase("https://doverweight.firebaseio.com/");

    }
    public void mostrarContraseña(View v){
        // Salvar cursor
        int cursor = TextPassword.getSelectionEnd();

        if(opcionMostrar.isChecked()){
            TextPassword.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }else{
            TextPassword.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }

        // Restaurar cursor
        TextPassword.setSelection(cursor);
    }


    public String getRandomString() {
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }

    private void registrarUsuario(){
        final String name = Textnombre.getText().toString().trim();
        final String apellido = textApellido.getText().toString().trim();
        final String edad = textEdad.getText().toString().trim();
        final String email = TextEmail.getText().toString().trim();
        final String password = TextPassword.getText().toString().trim();
        final String peso = textPeso.getText().toString().trim();
        final String estatura = textEstatura.getText().toString().trim();

        //Verificamos que las cajas de texto no esten vacías
        if(TextUtils.isEmpty(name)){
            Toast.makeText(this,"Se debe ingresar el nombre",Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(apellido)){
            Toast.makeText(this,"Se debe ingresar el apellido",Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(edad)){
            Toast.makeText(this,"Ingrese su edad",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Se debe ingresar un email",Toast.LENGTH_LONG).show();
                return;
        }

        if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Falta ingresar la contraseña",Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(peso)){
            Toast.makeText(this,"Se debe ingresar el peso",Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(estatura)){
            Toast.makeText(this,"Se debe ingresar el estatura",Toast.LENGTH_LONG).show();
            return;
        }


        progressDialog.setMessage("Realizando registro en linea...");
        progressDialog.show();

        //creating a new user
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if(task.isSuccessful()){
                            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("users");
                            DatabaseReference currentUserDB = mDatabase.child(firebaseAuth.getCurrentUser().getUid());

                            currentUserDB.child("name").setValue(name);
                            currentUserDB.child("apellido").setValue(apellido);
                            currentUserDB.child("edad").setValue(edad);
                            currentUserDB.child("email").setValue(email);
                            currentUserDB.child("peso").setValue(peso);
                            currentUserDB.child("estatura").setValue(estatura);

                            Toast.makeText(CrearCuenta.this,
                                    "Se ha registrado el usuario con el email: "+ TextEmail.getText(),Toast.LENGTH_LONG).show();
                        }else{

                            Toast.makeText(CrearCuenta.this,
                                    "No se pudo registrar el usuario ",Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                });
        textEdad.setText("");
        textApellido.setText("");
        TextPassword.setText("");
        TextEmail.setText("");
        Textnombre.setText("");
        textEstatura.setText("");
        textPeso.setText("");
    }
    @Override
    public void onClick(View v) {
        registrarUsuario();
    }

}
