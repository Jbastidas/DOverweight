package com.example.pc.doverweight.Interfaces;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.pc.doverweight.R;
import com.example.pc.doverweight.entidades.Usuario;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Perfil extends AppCompatActivity {
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener Auth;
    private DatabaseReference users;
    private String userId;
    private ListView mListView;
    int TYPE_TEXT_VARIATION_PASSWORD;
    TextView TextEmail, Textnombre, textApellido, textEdad,textPeso,textEstatura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        TextEmail = findViewById(R.id.txtCorreoCP);
        Textnombre= findViewById(R.id.txtNombreP);
        textApellido= findViewById(R.id.txtApellidoP);
        textEdad= findViewById(R.id.txtEdadP);
        textPeso= findViewById(R.id.txtPesoP);
        textEstatura= findViewById(R.id.txtEstaturaP);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        users = firebaseDatabase.getReference();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();
        users.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showData(dataSnapshot);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        recibirdatosPerfil();

    }

    private void showData(DataSnapshot dataSnapshot) {
        for(DataSnapshot ds : dataSnapshot.getChildren()){
            Usuario usuario = new Usuario();
            String nombre = usuario.setName(ds.child(userId).getValue(Usuario.class).getName());
            String apellido = usuario.setApellido(ds.child(userId).getValue(Usuario.class).getApellido());
            String edad = usuario.setEdad(ds.child(userId).getValue(Usuario.class).getEdad());
            String email = usuario.setEmail(ds.child(userId).getValue(Usuario.class).getEmail());
            String peso =usuario.setPeso(ds.child(userId).getValue(Usuario.class).getPeso());
            String estatura = usuario.setEstatura(ds.child(userId).getValue(Usuario.class).getEstatura());

            SharedPreferences sharedPreferences = getSharedPreferences("datosperfil", MODE_PRIVATE);
            SharedPreferences.Editor editor= sharedPreferences.edit();
            editor.putString("nombreP", String.valueOf(nombre));
            editor.putString("apellidoP", String.valueOf(apellido));
            editor.putString("pesoP", String.valueOf(peso));
            editor.putString("edadP", String.valueOf(edad));
            editor.putString("estaturaP", String.valueOf(estatura));
            editor.putString("correoP", String.valueOf(email));
            editor.commit();
        }
    }
    public void recibirdatosPerfil(){
        SharedPreferences sharedPreferences =
                getSharedPreferences("datosperfil", Context.MODE_PRIVATE);
        String edadS = sharedPreferences.getString("edadP","");
        String pesoS = sharedPreferences.getString("pesoP","");
        String estaturaS = sharedPreferences.getString("estaturaP","");
        String nombreS = sharedPreferences.getString("nombreP","");
        String apellidoS = sharedPreferences.getString("apellidoP","");
        String emailS = sharedPreferences.getString("correoP","");

        Textnombre.setText("Nombres: "+nombreS );
        TextEmail.setText("Correo: "+emailS);
        textEdad.setText("Edad: "+edadS+" años");
        textEstatura.setText("Estatura: "+estaturaS+" m");
        textPeso.setText("Peso: "+pesoS+" kg");
        textApellido.setText("Apellido: "+apellidoS);
    }
}
