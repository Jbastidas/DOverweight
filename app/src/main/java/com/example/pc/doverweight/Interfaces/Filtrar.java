package com.example.pc.doverweight.Interfaces;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.pc.doverweight.R;
import com.example.pc.doverweight.SqliteHelper.ConexionSQliteHelperCaloriasT;
import com.example.pc.doverweight.entidades.Almuerzos;
import com.example.pc.doverweight.entidades.CaloriasTotales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Filtrar extends AppCompatActivity {
    List<String> x = new ArrayList();
    ListView textView;
    TextView textViewCalorias;
    ArrayAdapter<String> adaptador;
    ArrayList<String> nombre = new ArrayList();
    ArrayList<String> nombre2 = new ArrayList();
    ArrayList<String> calorias = new ArrayList();
    ArrayList<String> array3 = new ArrayList();
    int sum;
    int sumtt, sumttt;
    ArrayList<CaloriasTotales> listaDesayunos;
    ConexionSQliteHelperCaloriasT conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtrar);
        textView= (ListView) findViewById(R.id.TextViewLista);
        textViewCalorias= findViewById(R.id.TextCalDia);
        nombre = (ArrayList<String>) getIntent().getSerializableExtra("miLista");
        calorias = (ArrayList<String>) getIntent().getSerializableExtra("miLista2");

        ObtenerDatosT();
        SumaCal();
        Datos();


    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.iconsig, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_sig) {
            Intent intent = new Intent(this, EstadoDieta.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
    public void ObtenerDatosT() {

        int length = nombre.size();
        if (length != calorias.size()) {
        }
        for (int i = 0; i < length; i++) {
            array3.add(nombre.get(i) + "\n " + calorias.get(i) + "  kcal");
        }
        adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array3);
        textView.setAdapter(adaptador);
    }
    public void SumaCal(){

        int o;

        for(o = 0; o < calorias.size(); o++){
            sum += Integer.parseInt(calorias.get(o));

        }
        SharedPreferences sharedPreferences = getSharedPreferences("calMomento", MODE_PRIVATE);
        textViewCalorias.setText("Calorias: "+ sum +"  kcal");
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("CaloriasMomento", String.valueOf(sum));
        editor.commit();
    }
    public void Datos(){
        ConexionSQliteHelperCaloriasT conn = new ConexionSQliteHelperCaloriasT(this, "caloriast", null, 1);
        SQLiteDatabase db = conn.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CaloriasTotales.CAMPO_CALORIAS, sum);
        long idResultante=db.insert(CaloriasTotales.NOMBRE_TABLA_CALORIAST, CaloriasTotales.CAMPO_CALORIAS, values);
    }


}


