package com.example.pc.doverweight.SqliteHelper;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pc.doverweight.entidades.Almuerzos;
import com.example.pc.doverweight.entidades.Desayunos;
import com.example.pc.doverweight.entidades.Meriendas;

public class ConexionSQliteHelperMer extends SQLiteOpenHelper {

    public ConexionSQliteHelperMer(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Meriendas.CREAR_TABLA_MERIENDAS);

        mockData(db);
    }
    //metodo mock para crear los registros de cada alimento
    private void mockData(SQLiteDatabase sqLiteDatabase) {
        mockLawyer(sqLiteDatabase, new Meriendas ("Hot dog",242 ));
        mockLawyer(sqLiteDatabase, new Meriendas ("Jamon de pavo",35 ));
        mockLawyer(sqLiteDatabase, new Meriendas ("Jamon serrano",181 ));
        mockLawyer(sqLiteDatabase, new Meriendas ("Papas fritas",312 ));
        mockLawyer(sqLiteDatabase, new Meriendas ("Pizza de peperoni",313 ));
        mockLawyer(sqLiteDatabase, new Meriendas ("Pizza de queso",285 ));
        mockLawyer(sqLiteDatabase, new Meriendas ("Salchicha de carne", 273));
        mockLawyer(sqLiteDatabase, new Meriendas ("Salchicha de cerdo", 181));
        mockLawyer(sqLiteDatabase, new Meriendas ("Salchicha de pavo", 88));
        mockLawyer(sqLiteDatabase, new Meriendas ("Salchicha plumrose", 120));
        mockLawyer(sqLiteDatabase, new Meriendas ("Salchicha de soya",95 ));
        mockLawyer(sqLiteDatabase, new Meriendas ("Tortilla de maiz",150 ));
        mockLawyer(sqLiteDatabase, new Meriendas ("Tortilla de papa",126 ));
        mockLawyer(sqLiteDatabase, new Meriendas ("Tortilla de yuca",280 ));
    }
    //insertar los alimentos en los registros de mockData
    public long mockLawyer(SQLiteDatabase db, Meriendas meriendas) {
        return db.insert(Meriendas.NOMBRE_TABLA_MERIENDAS,null,meriendas.toContentValues());
    }


    //metodo para utilizar la base de datos ya ingresada
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS alimentos");
        onCreate(db);

    }
    public long saveAlimentos (Desayunos alimentos){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.insert(Meriendas.NOMBRE_TABLA_MERIENDAS,null,alimentos.toContentValues());
    }
}
