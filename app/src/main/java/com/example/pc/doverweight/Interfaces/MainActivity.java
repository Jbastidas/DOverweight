package com.example.pc.doverweight.Interfaces;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.pc.doverweight.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener Auth;
    private ListView listViewCategorias;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();



        Auth = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user!=null){

                }else{
                    //error
                }

            }
        };

         Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
//
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        datosInicio();


        listViewCategorias.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        if (position == 0){
                            Intent myIntent = new Intent(view.getContext(), c_Desayuno.class);
                            startActivity(myIntent);
                        }else if(position == 1){
                            Intent myIntent = new Intent(view.getContext(), c_Almuerzos.class);
                            startActivity(myIntent);
                        }else if(position == 2){
                            Intent myIntent = new Intent(view.getContext(), c_Merienda.class);
                            startActivity(myIntent);
                        }else if(position == 3) {
                            Intent myIntent = new Intent(view.getContext(), c_Postres.class);
                            startActivity(myIntent);
                        }else if(position == 4) {
                            Intent myIntent = new Intent(view.getContext(), c_Bebida.class);
                            startActivity(myIntent);
                        }
                    }
                }
        );
    }
    public void datosInicio(){
        listViewCategorias = (ListView) findViewById(R.id.listViewProduct);
        ArrayList<Product> products = new ArrayList<>();

        products.add(new Product("Desayuno", "El desayuno es la primera comida que se consume en el día, y su consumo es por la mañana ", R.drawable.iconodesayuno));
        products.add(new Product("Almuerzo", "Alimento que se toma a mediodía, generalmente es el principal y más completo del día", R.drawable.inconoalmuerzo));
        products.add(new Product("Merienda", "Alimento generalmente ligero que se toma a media tarde, entre la comida y la cena.", R.drawable.iconomerienda));
        products.add(new Product("Postres","Alimento, generalmente dulce, que se sirve al finalizar una comida.",  R.drawable.iconopostres));
        products.add(new Product("Bebidas","La palabra bebida es una palabra de uso común que se refiere a todo tipo de líquidos que puedan ser utilizados para el consumo",  R.drawable.iconobebida));

        listViewCategorias.setAdapter(new ListProductAdapter(this, products));

    }



    static class ListProductAdapter extends BaseAdapter {
        private final Context context;
        private final ArrayList<Product> products;

        public ListProductAdapter(Context context, ArrayList<Product> products) {
            this.context = context;
            this.products = products;
        }

        @Override
        public int getCount() {
            return products.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

            Product product = products.get(position);
            ListViewHolder holder;
            if (view == null) {
                view = inflater.inflate(R.layout.categoria_row, viewGroup, false);
                holder = new ListViewHolder();
                holder.txtName = (TextView) view.findViewById(R.id.txtCategoria);
                holder.imgProduct = (ImageView) view.findViewById(R.id.imgCat);
                holder.txtDescripcion = (TextView) view.findViewById(R.id.txtCategoria2);

                view.setTag(holder);
            } else {
                Log.d("ListView", "RECYCLED");
                holder = (ListViewHolder) view.getTag();
            }

            holder.txtName.setText(product.name);
            holder.imgProduct.setImageResource(product.image);
            holder.txtDescripcion.setText(product.datos);
            return view;
        }

        static class ListViewHolder {
            TextView txtName;
            TextView txtDescripcion;
            ImageView imgProduct;
        }
    }
    public class Product {
        String name, datos;
        int image;

        public Product(String name, String datos, int image) {
            this.name = name;
            this.image = image;
            this.datos = datos;
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_iniciarDieta) {
             Intent estado = new Intent(this, EstadoDieta.class);
            startActivity(estado);
            finish();

        } else if (id == R.id.nav_perfil) {
            Intent estado = new Intent(MainActivity.this, Perfil.class);
            startActivity(estado);

        } else if (id == R.id.nav_ingresarAlimentos) {
            Intent estado = new Intent(MainActivity.this, NuevoAlimento.class);
            startActivity(estado);

        } else if (id == R.id.nav_desa) {
            Intent estado = new Intent(MainActivity.this, c_Desayuno.class);
            startActivity(estado);
        } else if (id == R.id.nav_almu) {
            Intent estado = new Intent(MainActivity.this, c_Almuerzos.class);
            startActivity(estado);
        } else if (id == R.id.nav_meri) {
            Intent estado = new Intent(MainActivity.this, c_Merienda.class);
            startActivity(estado);
        } else if (id == R.id.nav_pos) {
            Intent estado = new Intent(MainActivity.this, c_Postres.class);
            startActivity(estado);
        } else if (id == R.id.nav_beb) {
            Intent estado = new Intent(MainActivity.this, c_Bebida.class);
            startActivity(estado);

        } else if (id == R.id.nav_salir) {
            firebaseAuth.signOut();
            Intent intent = new Intent(MainActivity.this, PantallaInicial.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(Auth);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Auth != null) {
            firebaseAuth.removeAuthStateListener(Auth);
        }
    }

}
