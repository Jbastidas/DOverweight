package com.example.pc.doverweight.SqliteHelper;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pc.doverweight.entidades.Bebidas;
import com.example.pc.doverweight.entidades.Desayunos;

public class ConexionSQliteHelperBe extends SQLiteOpenHelper {

    public ConexionSQliteHelperBe(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Bebidas.CREAR_TABLA_BEBIDAS);

        mockData(db);
    }
    //metodo mock para crear los registros de cada alimento
    private void mockData(SQLiteDatabase sqLiteDatabase) {
        mockLawyer(sqLiteDatabase, new Bebidas ("Agua de coco", 21 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Batido de avena",333 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Batido de fresa",226 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Batido de guineo con leche", 140 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Colada morada", 183 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Jugo de frutilla",60 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Jugo de limon", 22 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Jugo de maracuya", 53 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Jugo de melon", 72 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Jugo de mora",95 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Jugo de naranjilla", 45 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Jugo de tomate de arbol", 60 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Leche", 149 ));
        mockLawyer(sqLiteDatabase, new Bebidas ("Leche con chocolate", 208 ));
    }
    //insertar los alimentos en los registros de mockData
    public long mockLawyer(SQLiteDatabase db, Bebidas bebidas) {
        return db.insert(Bebidas.NOMBRE_TABLA_BEBIDAS,null,bebidas.toContentValues());
    }


    //metodo para utilizar la base de datos ya ingresada
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS alimentos");
        onCreate(db);

    }
    public long saveAlimentos (Desayunos alimentos){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.insert(Bebidas.NOMBRE_TABLA_BEBIDAS,null,alimentos.toContentValues());
    }
}
