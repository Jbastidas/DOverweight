package com.example.pc.doverweight.SqliteHelper;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pc.doverweight.entidades.Desayunos;

public class ConexionSQliteHelper extends SQLiteOpenHelper {

    public ConexionSQliteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Desayunos.CREAR_TABLA_DESAYUNOS);

        mockData(db);
    }
    //metodo mock para crear los registros de cada alimento
    private void mockData(SQLiteDatabase sqLiteDatabase) {
        mockLawyer(sqLiteDatabase, new Desayunos ("Cafe con azucar",80 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Chocolate en polvo", 147 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Espinaca cocinada",41 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Gelatina de fresa",57 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Gelatina sin sabor",9 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Guineo o banana", 89));
        mockLawyer(sqLiteDatabase, new Desayunos ("Huevo frito",241 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Huevo hervido",155 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Mandarina",37 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Manzana", 52 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Naranja",47 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Pan blanco",74 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Pera",101 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Platanos fritos",497 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Queso fresco",310 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Sandia",30 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Tortilla de harina",316 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Tortilla de platano con queso",240 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Tostada con pavo y queso", 350));
        mockLawyer(sqLiteDatabase, new Desayunos ("Tostada de jamon y queso", 136));
        mockLawyer(sqLiteDatabase, new Desayunos ("Yogurt con frutas",243 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Yogurt de vainilla", 129));
        mockLawyer(sqLiteDatabase, new Desayunos ("Yogurt natural", 63));
        mockLawyer(sqLiteDatabase, new Desayunos ("Yogurt desnatado",59 ));
        mockLawyer(sqLiteDatabase, new Desayunos ("Zanahoria", 25));
    }
    //insertar los alimentos en los registros de mockData
    public long mockLawyer(SQLiteDatabase db, Desayunos alimentos) {
        return db.insert(Desayunos.NOMBRE_TABLA_DESAYUNOS,null,alimentos.toContentValues());
    }


    //metodo para utilizar la base de datos ya ingresada
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS alimentos");
        onCreate(db);

    }
    public long saveAlimentos (Desayunos alimentos){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.insert(Desayunos.NOMBRE_TABLA_DESAYUNOS,null,alimentos.toContentValues());
    }
}
