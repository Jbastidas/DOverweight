package com.example.pc.doverweight.entidades;

import android.content.ContentValues;


public class Desayunos {
    public static final String NOMBRE_TABLA_DESAYUNOS = "desayunos";
    public static final String CAMPO_NOMBRE = "nombre";
    public static final String CAMPO_CALORIAS = "calorias";

    public static final String CREAR_TABLA_DESAYUNOS = "CREATE TABLE "
            + NOMBRE_TABLA_DESAYUNOS + "(" + CAMPO_NOMBRE + " TEXT, "
            + CAMPO_CALORIAS + " TEXT)";


    String nombre;
    int calorias;

    public Desayunos(String nombre, int calorias) {
        this.nombre = nombre;
        this.calorias = calorias;
    }

    public Desayunos() {
        this.nombre = nombre;
        this.calorias = calorias;
    }


    public String getNombre() {
        return nombre;
    }

    public String setNombre(String nombre) {
        this.nombre = nombre;
        return nombre;
    }

    public int getCalorias() {
        return calorias;
    }

    public int setCalorias(int calorias) {
        this.calorias = calorias;
        return calorias;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(CAMPO_NOMBRE, nombre);
        values.put(CAMPO_CALORIAS, calorias);
        return  values;
    }


}
