package com.example.pc.doverweight.entidades;

import android.content.ContentValues;

public class CaloriasTotales {

    public static final String NOMBRE_TABLA_CALORIAST = "caloriast";
    public static final String CAMPO_CALORIAS = "calorias";

    public static final String CREAR_TABLA_CALORIAST = "CREATE TABLE "
            + NOMBRE_TABLA_CALORIAST  + "("+ CAMPO_CALORIAS + " TEXT);";


    int calorias;

    public int getCalorias() {
        return calorias;
    }

    public int setCalorias(int calorias) {
        this.calorias = calorias;
        return calorias;
    }

    public CaloriasTotales(int calorias) {

        this.calorias = calorias;
    }
    public CaloriasTotales() {

        this.calorias = calorias;
    }
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(CAMPO_CALORIAS, calorias);
        return  values;
    }
}
