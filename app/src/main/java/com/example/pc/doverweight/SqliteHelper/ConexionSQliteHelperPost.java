package com.example.pc.doverweight.SqliteHelper;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pc.doverweight.entidades.Almuerzos;
import com.example.pc.doverweight.entidades.Desayunos;
import com.example.pc.doverweight.entidades.Meriendas;
import com.example.pc.doverweight.entidades.Postres;

public class ConexionSQliteHelperPost extends SQLiteOpenHelper {

    public ConexionSQliteHelperPost(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Postres.CREAR_TABLA_POSTRES);

        mockData(db);
    }
    //metodo mock para crear los registros de cada alimento
    private void mockData(SQLiteDatabase sqLiteDatabase) {
        mockLawyer(sqLiteDatabase, new Postres ("Helado de vainilla",108 ));
        mockLawyer(sqLiteDatabase, new Postres ("Helado de yogurt", 234 ));
        mockLawyer(sqLiteDatabase, new Postres ("Pastel de manzana",331 ));
        mockLawyer(sqLiteDatabase, new Postres ("Pastelillo de queso",180 ));
        mockLawyer(sqLiteDatabase, new Postres ("Pie de limon",404 ));
        mockLawyer(sqLiteDatabase, new Postres ("Pie de manzana",237 ));
        mockLawyer(sqLiteDatabase, new Postres ("Torta de 3 leches",375 ));
        mockLawyer(sqLiteDatabase, new Postres ("Torta de choclo",325 ));
        mockLawyer(sqLiteDatabase, new Postres ("Torta de chocolate",358 ));
        mockLawyer(sqLiteDatabase, new Postres ("Torta de maduro",1284 ));
        mockLawyer(sqLiteDatabase, new Postres ("Torta de milhojas",397 ));
        mockLawyer(sqLiteDatabase, new Postres ("Torta de platano",400 ));
        mockLawyer(sqLiteDatabase, new Postres ("Torta de queso",321 ));
    }
    //insertar los alimentos en los registros de mockData
    public long mockLawyer(SQLiteDatabase db, Postres postres) {
        return db.insert(Postres.NOMBRE_TABLA_POSTRES,null,postres.toContentValues());
    }


    //metodo para utilizar la base de datos ya ingresada
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS alimentos");
        onCreate(db);

    }
    public long saveAlimentos (Desayunos alimentos){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.insert(Postres.NOMBRE_TABLA_POSTRES,null,alimentos.toContentValues());
    }
}
