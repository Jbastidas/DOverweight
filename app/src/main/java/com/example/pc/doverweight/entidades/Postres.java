package com.example.pc.doverweight.entidades;

import android.content.ContentValues;


public class Postres {
    public static final String NOMBRE_TABLA_POSTRES = "postres";
    public static final String CAMPO_NOMBRE = "nombre";
    public static final String CAMPO_CALORIAS = "calorias";

    public static final String CREAR_TABLA_POSTRES = "CREATE TABLE "
            + NOMBRE_TABLA_POSTRES + "(" + CAMPO_NOMBRE + " TEXT, "
            + CAMPO_CALORIAS + " TEXT)";


    String nombre;
    int calorias;

    public Postres(String nombre, int calorias) {
        this.nombre = nombre;
        this.calorias = calorias;
    }

    public Postres() {
        this.nombre = nombre;
        this.calorias = calorias;
    }


    public String getNombre() {
        return nombre;
    }

    public String setNombre(String nombre) {
        this.nombre = nombre;
        return nombre;
    }

    public int getCalorias() {
        return calorias;
    }

    public int setCalorias(int calorias) {
        this.calorias = calorias;
        return calorias;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(CAMPO_NOMBRE, nombre);
        values.put(CAMPO_CALORIAS, calorias);
        return  values;
    }

}
