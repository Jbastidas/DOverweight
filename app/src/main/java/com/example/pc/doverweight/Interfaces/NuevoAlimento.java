package com.example.pc.doverweight.Interfaces;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.pc.doverweight.R;
import com.example.pc.doverweight.SqliteHelper.ConexionSQliteHelper;
import com.example.pc.doverweight.SqliteHelper.ConexionSQliteHelperAl;
import com.example.pc.doverweight.SqliteHelper.ConexionSQliteHelperBe;
import com.example.pc.doverweight.SqliteHelper.ConexionSQliteHelperMer;
import com.example.pc.doverweight.SqliteHelper.ConexionSQliteHelperPost;
import com.example.pc.doverweight.entidades.Almuerzos;
import com.example.pc.doverweight.entidades.Bebidas;
import com.example.pc.doverweight.entidades.Desayunos;
import com.example.pc.doverweight.entidades.Meriendas;
import com.example.pc.doverweight.entidades.Postres;
import com.google.firebase.auth.FirebaseAuth;


public class NuevoAlimento extends AppCompatActivity {

    Button buttonGuardar;
    EditText nombre, calorias;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    Spinner spinner;
    RadioButton buttonDesayunos, buttonAlmuerzos, buttonMeriendas, buttonPostres, buttonBebidas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_alimento);
        buttonGuardar= findViewById(R.id.btnGuardar);
        nombre=findViewById(R.id.txtNombreAlimento);
        calorias=findViewById(R.id.txtCalorias);
        progressDialog = new ProgressDialog(this);
        buttonAlmuerzos = findViewById(R.id.radio_almuerzos);
        buttonDesayunos = findViewById(R.id.radio_desayunos);
        buttonMeriendas = findViewById(R.id.radio_meriendas);
        buttonPostres = findViewById(R.id.radio_postres);
        buttonBebidas= findViewById(R.id.radio_bebidas);



        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarAlimentos();
            }
        });
    }

    private void registrarAlimentos() {
        final String name = nombre.getText().toString().trim();
        final String calori = calorias.getText().toString().trim();


        if (TextUtils.isEmpty(name)){
            Toast.makeText(this, "Ingrese el Nombre", Toast.LENGTH_SHORT).show();
            return;
        }if (TextUtils.isEmpty(calori)) {
            Toast.makeText(this, "Ingrese el Calorias", Toast.LENGTH_SHORT).show();
            return;
        }
            if (buttonPostres.isChecked()==true) {
                ConexionSQliteHelperPost conn = new ConexionSQliteHelperPost(this, "postres", null, 1);
                SQLiteDatabase db = conn.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(Postres.CAMPO_NOMBRE, name);
                values.put(Postres.CAMPO_CALORIAS, calori);
                long idResultante=db.insert(Postres.NOMBRE_TABLA_POSTRES, Postres.CAMPO_NOMBRE, values);
                Toast.makeText(this, "Alimento: " + name +" guardado", Toast.LENGTH_SHORT).show();
            } else
            if (buttonMeriendas.isChecked()==true) {
                ConexionSQliteHelperMer conn = new ConexionSQliteHelperMer(this, "meriendas", null, 1);
                SQLiteDatabase db = conn.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(Meriendas.CAMPO_NOMBRE, name);
                values.put(Meriendas.CAMPO_CALORIAS, calori);
                long idResultante=db.insert(Meriendas.NOMBRE_TABLA_MERIENDAS, Meriendas.CAMPO_NOMBRE, values);
                Toast.makeText(this, "Alimento: " + name +" guardado", Toast.LENGTH_SHORT).show();

            }
            else
            if (buttonDesayunos.isChecked()==true) {
                ConexionSQliteHelper conn = new ConexionSQliteHelper(this, "desayunos", null, 1);
                SQLiteDatabase db = conn.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(Desayunos.CAMPO_NOMBRE, name);
                values.put(Desayunos.CAMPO_CALORIAS, calori);
                long idResultante=db.insert(Desayunos.NOMBRE_TABLA_DESAYUNOS, Desayunos.CAMPO_NOMBRE, values);
                Toast.makeText(this, "Alimento: " + name +" guardado", Toast.LENGTH_SHORT).show();


            }else
            if (buttonAlmuerzos.isChecked()==true) {
                ConexionSQliteHelperAl conn = new ConexionSQliteHelperAl(this, "almuerzos", null, 1);
                SQLiteDatabase db = conn.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(Almuerzos.CAMPO_NOMBRE, name);
                values.put(Almuerzos.CAMPO_CALORIAS, calori);
                long idResultante=db.insert(Almuerzos.NOMBRE_TABLA_ALMUERZOS, Almuerzos.CAMPO_NOMBRE, values);
                Toast.makeText(this, "Alimento: " + name +" guardado", Toast.LENGTH_SHORT).show();


            }
        if (buttonBebidas.isChecked()==true) {
            ConexionSQliteHelperBe conn = new ConexionSQliteHelperBe(this, "bebidas", null, 1);
            SQLiteDatabase db = conn.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Bebidas.CAMPO_NOMBRE, name);
            values.put(Bebidas.CAMPO_CALORIAS, calori);
            long idResultante=db.insert(Bebidas.NOMBRE_TABLA_BEBIDAS, Bebidas.CAMPO_NOMBRE, values);
            Toast.makeText(this, "Alimento: " + name +" guardado", Toast.LENGTH_SHORT).show();


        }
        nombre.setText("");
        calorias.setText("");

    }
}
