package com.example.pc.doverweight.Interfaces;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pc.doverweight.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity implements View.OnClickListener {
    Button buttonIngresar, buttonCrearCuenta;
    EditText correoI, contraseñaI;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private CheckBox opcionMostrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        buttonIngresar = findViewById(R.id.btnIngresar);
        buttonCrearCuenta = findViewById(R.id.btnPantallaCrearC);
        correoI = findViewById(R.id.txtCorreo);
        contraseñaI = findViewById(R.id.txtContra);
        opcionMostrar = (CheckBox)findViewById(R.id.opcion_mostrar);


        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        buttonCrearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, CrearCuenta.class);
                startActivity(intent);
            }
        });
        buttonIngresar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        ingresarCuenta();
    }
    public void mostrarContraseña(View v){
        // Salvar cursor
        int cursor = contraseñaI.getSelectionEnd();

        if(opcionMostrar.isChecked()){
            contraseñaI.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }else{
            contraseñaI.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }

        // Restaurar cursor
        contraseñaI.setSelection(cursor);
    }
    public void ingresarCuenta(){
        final String email = correoI.getText().toString().trim();
        String password = contraseñaI.getText().toString().trim();
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Se debe ingresar un email",
                    Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Falta ingresar la contraseña",
                    Toast.LENGTH_LONG).show();
            return;
        }
        progressDialog.setMessage("Iniciando Sesion");
        progressDialog.show();
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            int pos = email.indexOf("@");
                            String usuario = email.substring(0, pos);
                            Toast.makeText(Login.this, "Bienvenido: " + correoI.getText(),
                                    Toast.LENGTH_LONG).show();
                            Intent intencion = new Intent(getApplication(), MainActivity.class);


                            startActivity(intencion);
                        } else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                Toast.makeText(Login.this, "Ese usuario ya existe ",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Login.this, "No se pudo registrar el usuario ",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                        progressDialog.dismiss();
                    }
                });
    }
}
