package com.example.pc.doverweight.SqliteHelper;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pc.doverweight.entidades.Almuerzos;
import com.example.pc.doverweight.entidades.Desayunos;

public class ConexionSQliteHelperAl extends SQLiteOpenHelper{

    public ConexionSQliteHelperAl(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Almuerzos.CREAR_TABLA_ALMUERZOS);

        mockData(db);
    }
    //metodo mock para crear los registros de cada alimento
    private void mockData(SQLiteDatabase sqLiteDatabase) {
        mockLawyer(sqLiteDatabase, new Almuerzos ("Arroz blanco cocinado", 205));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Arroz con atun", 166 ));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Arroz con carne frita",400 ));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Arroz con huevo frito",300 ));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Arroz con lenteja",193 ));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Arroz con longaniza", 400 ));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Arroz con verduras",95 ));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Arroz con pollo", 350));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Arroz con pulpo", 150));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Ensalada de atun", 383 ));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Pure de papas",215 ));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Sopa de fideos",300 ));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Sopa de pollo",174 ));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Sopa de verduras",100 ));
        mockLawyer(sqLiteDatabase, new Almuerzos ("Sopa de verduras y carne",186 ));
    }
    //insertar los alimentos en los registros de mockData
    public long mockLawyer(SQLiteDatabase db, Almuerzos almuerzos) {
        return db.insert(Almuerzos.NOMBRE_TABLA_ALMUERZOS,null,almuerzos.toContentValues());
    }


    //metodo para utilizar la base de datos ya ingresada
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS alimentos");
        onCreate(db);

    }
    public long saveAlimentos (Desayunos alimentos){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.insert(Almuerzos.NOMBRE_TABLA_ALMUERZOS,null,alimentos.toContentValues());
    }
}
