package com.example.pc.doverweight.Interfaces;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.pc.doverweight.R;
import com.example.pc.doverweight.SqliteHelper.ConexionSQliteHelperMer;
import com.example.pc.doverweight.entidades.Meriendas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class c_Merienda extends AppCompatActivity {
    ArrayList<String> listaCalorias;
    ArrayList<Meriendas> listaDesayunos;
    List<String> x = new ArrayList();
    List<String> y = new ArrayList();
    ListView lv1;
    ConexionSQliteHelperMer conn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c__merienda);
        conn = new ConexionSQliteHelperMer(getApplicationContext(), "meriendas", null, 1);
        datosInicio();
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Se busca la referencia del TextView en la vista.
                TextView textView = (TextView) view.findViewById(R.id.txtNombreRaw);
                TextView textViewcal = (TextView) view.findViewById(R.id.txtCaloriasRaw);
                //Obtiene el texto dentro del TextView.
                String textItemList  = textView.getText().toString();
                String textItemListcal  = textViewcal.getText().toString();


                x.add(String.valueOf(textItemList));
                y.add(textItemListcal);
            }
        });

    }



    public void datosInicio(){
        lv1 = (ListView) findViewById(R.id.r_Merienda);
        ArrayList<c_Merienda.Product> products = new ArrayList<>();
        SQLiteDatabase db=conn.getReadableDatabase();
        Meriendas meriendas=null;
        listaDesayunos=new ArrayList<Meriendas>();
        Cursor cursor=db.rawQuery("SELECT * FROM "+Meriendas.NOMBRE_TABLA_MERIENDAS,null);
        while (cursor.moveToNext()){
            Object name;
            meriendas=new Meriendas();
            String nombre = meriendas.setNombre(cursor.getString(0));
            int calori = meriendas.setCalorias(Integer.parseInt(cursor.getString(1)));
            listaDesayunos.add(meriendas);
            products.add(new Product(nombre, calori));
        }

        lv1.setAdapter(new ListProductAdapter(this, products));

    }

    static class ListProductAdapter extends BaseAdapter {
        private final Context context;
        private final ArrayList<c_Merienda.Product> products;

        public ListProductAdapter(Context context, ArrayList<c_Merienda.Product> products) {
            this.context = context;
            this.products = products;
        }

        @Override
        public int getCount() {
            return products.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

            c_Merienda.Product product = products.get(position);
            c_Merienda.ListProductAdapter.ListViewHolder holder;
            if (view == null) {
                view = inflater.inflate(R.layout.list_merienda_row, viewGroup, false);
                holder = new ListProductAdapter.ListViewHolder();
                holder.txtName = (TextView) view.findViewById(R.id.txtNombreRaw);
                holder.txtDescripcion = (TextView) view.findViewById(R.id.txtCaloriasRaw);

                view.setTag(holder);
            } else {
                Log.d("ListView", "RECYCLED");
                holder = (c_Merienda.ListProductAdapter.ListViewHolder) view.getTag();
            }

            holder.txtName.setText(product.name);
            holder.txtDescripcion.setText(Integer.toString(product.datos));
            return view;
        }

        static class ListViewHolder {
            TextView txtName;
            TextView txtDescripcion;
        }
    }
    public class Product {
        String name;
        int datos;

        public Product(String name, int datos) {
            this.name = name;
            this.datos = datos;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, NuevoAlimento.class);
            startActivity(intent);
        }
        if (id == R.id.action_sig) {
            Intent intent = new Intent(this, Filtrar.class);
            intent.putExtra("miLista", (Serializable) x);
            intent.putExtra("miLista2", (Serializable) y);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
