package com.example.pc.doverweight.SqliteHelper;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pc.doverweight.entidades.CaloriasTotales;
import com.example.pc.doverweight.entidades.Desayunos;

public class ConexionSQliteHelperCaloriasT extends SQLiteOpenHelper {

    public ConexionSQliteHelperCaloriasT(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CaloriasTotales.CREAR_TABLA_CALORIAST);

        mockData(db);
    }
    //metodo mock para crear los registros de cada alimento
    private void mockData(SQLiteDatabase sqLiteDatabase) {

    }
    //insertar los alimentos en los registros de mockData
    public long mockLawyer(SQLiteDatabase db, CaloriasTotales caloriasTotales) {
        return db.insert(CaloriasTotales.NOMBRE_TABLA_CALORIAST,null,caloriasTotales.toContentValues());
    }


    //metodo para utilizar la base de datos ya ingresada
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS alimentos");
        onCreate(db);

    }
    public long saveAlimentos (CaloriasTotales caloriasTotales){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.insert(CaloriasTotales.NOMBRE_TABLA_CALORIAST,null,caloriasTotales.toContentValues());
    }
}
